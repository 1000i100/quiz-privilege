# quiz-privilege

- [demo](https://1000i100.frama.io/quiz-privilege/privilege-short.html)

Un questionnaire pour sensibiliser aux privilèges, aux rapport de domination/oppression.
Aider aux prises de consciences, même sans intérêt initial pour le sujet.
